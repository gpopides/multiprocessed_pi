"""Main file."""
import sys
import multiprocessing
import math
import timeit


class Worker(object):
    """Worker class that deals with the calculation of pi."""
    #  def __init__(self, worker_id, start, end, total_steps, pi_array):
    def __init__(self, **kwargs):
        """Init."""
        self.start_b = int(kwargs['start'])
        self.end_b = int(kwargs['stop'])
        self.worker_id = kwargs['worker_id']
        self.t_steps = kwargs['total_steps']
        self.p_array = kwargs['pi_obj']

    def calculation(self):
        """Calculation formula and appending the result to the array."""
        step = float(1/self.t_steps)
        sum_pi = 0
        for i in range(self.start_b, self.end_b):
            x = (float(i) + 0.5) * step
            sum_pi += 4.0 / (1.0 + x * x)
        pi = sum_pi * step
        self.p_array.add_to_array(pi, self.worker_id)

    def start(self):
        """Start the calculation method attached to a thread."""
        process = multiprocessing.Process(target=self.calculation)
        process.start()
        process.join()


class PiArray():
    """
    Class that has a multiprocessed array object so values
    from different processes can be stored here
    """
    def __init__(self):
        self.pi_array = multiprocessing.Array('f', get_cores())

    def add_to_array(self, num, index):
        self.pi_array[index] = num


def get_cores():
    '''Return the cpu number of the machine'''
    return multiprocessing.cpu_count()


def init():
    '''Init.'''
    try:
        pi_object = PiArray()
        sum_pi = 0
        ts = int(sys.argv[1])  # total steps
        start = timeit.default_timer()
        num_of_threads = get_cores()
        workers = []
        block = ts / num_of_threads
        k = 0
        j = block

        for i in range(num_of_threads):
            worker = Worker(
                worker_id=i,
                start=k,
                stop=j,
                total_steps=ts,
                pi_obj=pi_object)

            workers.append(worker)
            workers[i].start()
            k += block
            j += block

        end = timeit.default_timer() - start

        sum_pi = [value for value in pi_object.pi_array]
        sum_pi = sum(sum_pi)

        difference = math.pi - sum_pi

        print(f'My PI: {sum_pi}.  Math PI:{math.pi} Difference:{difference}')
        print(f'Time needed {end}')
    except IndexError:
        print('Enter steps number')


if __name__ == '__main__':
    init()
